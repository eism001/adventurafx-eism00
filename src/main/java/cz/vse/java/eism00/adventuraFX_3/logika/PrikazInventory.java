package cz.vse.java.eism00.adventuraFX_3.logika;


public class PrikazInventory implements IPrikaz {

    private static final String NAZEV = "inventory";
    private Inventory inventory;

    public PrikazInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length != 0) {
            return "Špatný příkaz, použij napovedu";
        } else if(inventory.getPocetDusi() == 0){
            return "V inventáři zatím nic nemáš";
        } else if(inventory.getPocetDusi() == 1){
            return "V inventáři máš jednu duši";
        } else if(inventory.getPocetDusi() >= 2 && inventory.getPocetDusi() <= 4){
            return "V inventáři máš "+ inventory.getPocetDusi() + " duše.";
        } else {
            return "V inventáři máš "+ inventory.getPocetDusi() + " duší.";
        }
    }

    @Override
    public String getNazev() {
        return NAZEV;
    }
}