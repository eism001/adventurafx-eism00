package cz.vse.java.eism00.adventuraFX_3.logika;


public class PrikazStaty implements IPrikaz{

    private static final String NAZEV = "staty";
    private Postava postava;

    public PrikazStaty(Postava postava){
        this.postava = postava;
    }


    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length != 0) {
            return "Zadal jsi špatný přikaz, použij nápovědu.";
        }else {
            return "Máš "+ postava.getHealth() + " životů, " + postava.getMana()+ " many a "+ postava.getAtk()+ " poškození.";
        }
    }

    @Override
    public String getNazev() {
        return NAZEV;
    }
}
