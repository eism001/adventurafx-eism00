package cz.vse.java.eism00.adventuraFX_3.logika;

public class Inventory {

    private int pocetDusi;

    public Inventory(int pocetDusi) {
        this.pocetDusi = pocetDusi;
    }

    public int getPocetDusi() {
        return pocetDusi;
    }

    public void setPocetDusi(int pocetDusi) {
        this.pocetDusi = pocetDusi;
    }
}
