package cz.vse.java.eism00.adventuraFX_3.logika;

import cz.vse.java.eism00.adventuraFX_3.Main.Observer;
import cz.vse.java.eism00.adventuraFX_3.Main.Subject;

import java.util.HashSet;
import java.util.Set;

public class HerniPlan implements Subject {

    private Prostor aktualniProstor;
    private Set<Observer> observerList;

    /**
     *  Konstruktor který vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví halu.
     */
    public HerniPlan() {
        observerList= new HashSet<>();
        zalozProstoryHry();

    }
    /**
     *  Vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví domeček.
     */
    private void zalozProstoryHry() {
        // vytvářejí se jednotlivé prostory
        Prostor pokoj = new Prostor("pokoj","Hotelový pokoj, ve kterém se probudíš.");
        Prostor chodba = new Prostor("chodba", "Tmavá chodba hotelu, na jejíž konci jsou pouze jedny dveře");
        Prostor tajemnyLes = new Prostor("tajemnyLes","Prastarý les plný tajemství a monster.\n"+
                "V kořeni stromu vidíš zapíchnutý meč.");
        Prostor kraluvTrun = new Prostor("kraluvTrun","Jediná místnost, která zbyla uvnitř zbořeného hradu.");
        Prostor sneznaOblast = new Prostor("sneznaOblast","Zasněžená část země, která odděluje les od vodopádu.\n"+
                "V hordě sněhu vidíš odhozený štít.");
        Prostor vodopad = new Prostor("vodopad","Nekončící vodopád, který střeží zemi monster od lidí.");
        Prostor laborator = new Prostor("laborator",".laboratoř, ve které král nechává provádět experimenty na lidech.");
        Prostor trhlina = new Prostor("trhlina","Kouzelné místo, které slouží jako cesta mezi dvěma světy.");
        Prostor dvere = new Prostor("dvere","Dveře vedoucí zpátky do země lidí.");


        NPC flowey = new NPC("Flowey","Aaaaa nově spadlý človíček. Vítej v naší zemi. Já jsem Flowey.\n" +
                "Heh, chceš naučit základům této hry jo? To je mi ale smůla, že jsi narazil nejdřív na mě ;).\n" +
                "Jestli chceš projít dál, musíš si nejdřív poradit se mnou.",
                false, false,20,50, 1 );
        NPC asgore = new NPC("Král Asgore","Vítej v naší zemí človíčku. Dovol mi ti prvně vysvětlit pár věcí.\n" +
                "Víš, nám sem dolů občas nějaký člověk spadne, většinou se moc nezdrží a hned uhání domů.\n" +
                "Věřím však, že ty bys mohl být výjimka a pomoct nám. Stačí, když nasbíráš naše duše a dostaneš se do trhliny.\n" +
                "Nenech se zmást chováním ostatních monster, oni nejsou tak špatní, jak vypadají.",
                false, false,20,100, 1);
        NPC papyrus = new NPC("Papyrus","Cože?? Člověk?! Co tu dělá člověk?\n" +
                "Je docela vzácné vidět tu jednoho z vás. Měl by sis dát bacha na Undyne, ta zatím ulovila každého z Vás,co tu kdy skončil."
                , false, false,20,130,1);
        NPC undyne = new NPC("Undyne", "Na tenhle moment jsem čekala dlouho. Konečně, tohle si vychutnám.",false, false,25,170,1);
        NPC mettaton = new NPC("Mettaton", "Já jsem Mettaton, robot stvořen na chytání lidských duší pro krále.\n" +
                "Doufám, že jsi na mě připravený ;) "
                ,false,false,30,200,1);
        NPC sans = new NPC("Sans","Vidím, že se zbytkem z nás sis už poradil :)\n" +
                "Tak já už bych teď měl být pro tebe hračka. ;) "
                ,false,false,35,250,1);

        // přiřazují se průchody mezi prostory (sousedící prostory)
        pokoj.setVychod(chodba);
        chodba.setVychod(tajemnyLes);
        tajemnyLes.setVychod(kraluvTrun);
        kraluvTrun.setVychod(sneznaOblast);
        sneznaOblast.setVychod(vodopad);
        vodopad.setVychod(laborator);
        laborator.setVychod(trhlina);
        trhlina.setVychod(dvere);

        //nastaví NPC do dané lokace
        tajemnyLes.setNpcVMístnosti(flowey);
        kraluvTrun.setNpcVMístnosti(asgore);
        sneznaOblast.setNpcVMístnosti(papyrus);
        vodopad.setNpcVMístnosti(undyne);
        laborator.setNpcVMístnosti(mettaton);
        trhlina.setNpcVMístnosti(sans);

        //nastavi Vec do dané lokace
        tajemnyLes.vlozVec(new Vec("meč",true));
        tajemnyLes.vlozVec(new Vec("pařez",false));
        sneznaOblast.vlozVec(new Vec("štít",true));
        vodopad.vlozVec(new Vec("láhev vody",true));
        laborator.vlozVec(new Vec("stroj",false));


        aktualniProstor = pokoj;  // hra začíná v domečku
    }
    /**
     *  Metoda vrací odkaz na aktuální prostor, ve ktetém se hráč právě nachází.
     *
     *@return     aktuální prostor
     */

    public Prostor getAktualniProstor() {
        return aktualniProstor;
    }

    /**
     *  Metoda nastaví aktuální prostor, používá se nejčastěji při přechodu mezi prostory
     *
     *@param  prostor nový aktuální prostor
     */
    public void setAktualniProstor(Prostor prostor) {
        aktualniProstor = prostor;
        notifyObservers();
    }

    @Override
    public void register(Observer observer) {
        observerList.add(observer);
    }

    @Override
    public void unregister(Observer observer) {
        observerList.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer: observerList){
            observer.update();
        }

    }
    /**
     *  Metoda vrací odkaz na vítězný prostor.
     *
     *@return     vítězný prostor
     */


}

