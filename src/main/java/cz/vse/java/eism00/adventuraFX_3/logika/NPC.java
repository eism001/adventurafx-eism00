package cz.vse.java.eism00.adventuraFX_3.logika;

public class NPC {
    private String jmeno;
    private String dialog;
    private boolean probehlDialog;
    private boolean probehlFight;
    private int atk;
    private int health;
    private int darDuse;

    public NPC(final String jmeno, final String dialog, boolean probehlDialog, boolean probehlFight, int atk, int health, int darDuse){
        this.jmeno = jmeno;
        this.dialog = dialog;
        this.probehlDialog = probehlDialog;
        this.probehlFight = probehlFight;
        this.atk = atk;
        this.health = health;
        this.darDuse = darDuse;
    }

    public int getDarDuse() {
        return darDuse;
    }

    public void setDarDuse(int darDuse) {
        this.darDuse = darDuse;
    }

    public boolean isProbehlFight() {
        return probehlFight;
    }

    public void setProbehlFight(boolean probehlFight) {
        this.probehlFight = probehlFight;
    }


    public int getHealth() {
        return health;
    }

    public String getJmeno() {
        return jmeno;
    }

    public String getDialog() {
        return dialog;
    }

    public boolean isProbehlDialog() {
        return probehlDialog;
    }

    public int getAtk() {
        return atk;
    }


    public void setJmeno(String jmeno) {
        this.jmeno = jmeno;
    }

    public void setDialog(String dialog) {
        this.dialog = dialog;
    }

    public void setProbehlDialog(boolean probehlDialog) {
        this.probehlDialog = probehlDialog;
    }

    public void setAtk(int atk) {
        this.atk = atk;
    }


    public void setHealth(int health) {
        this.health = health;
    }
}
