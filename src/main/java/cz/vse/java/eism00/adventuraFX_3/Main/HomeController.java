package cz.vse.java.eism00.adventuraFX_3.Main;

import cz.vse.java.eism00.adventuraFX_3.logika.Hra;
import cz.vse.java.eism00.adventuraFX_3.logika.IHra;
import cz.vse.java.eism00.adventuraFX_3.logika.Prostor;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.control.ListView;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;


import java.util.HashMap;
import java.util.Map;

public class HomeController extends GridPane implements Observer {




    @FXML
    private ImageView lahevVody;
    @FXML
    private ImageView stit;
    @FXML
    private ImageView mec;
    @FXML
    private ImageView hrac;
    @FXML
    private ListView panelVychodu;
    @FXML
    private TextArea vystup;
    @FXML
    private TextField vstup;
    @FXML
    private Button odesli;

    private IHra hra;
    private Map<String, Point2D> souradniceProstoru= new HashMap<>();
    private final Map<String, ImageView> veci =new HashMap<>();

    @FXML
    public void initialize(){
        hra = new Hra();
        vystup.appendText(hra.vratUvitani()+"\n\n");
        vystup.setEditable(false);

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                vstup.requestFocus();
            }
        });

        hra.getHerniPlan().register(this);


        veci.put("meč", mec);
        veci.put("štít", stit);
        veci.put("láhev Vody", lahevVody);

        mec.setVisible(false);
        stit.setVisible(false);
        lahevVody.setVisible(false);

        souradniceProstoru= createSouradniceProstoru();
        update();

    }

    private Map<String, Point2D> createSouradniceProstoru(){
        Map<String, Point2D> souradniceProstoru = new HashMap<>();
        souradniceProstoru.put("pokoj",new Point2D(28,50));
        souradniceProstoru.put("chodba",new Point2D(164,50));
        souradniceProstoru.put("tajemnyLes",new Point2D(331,43));
        souradniceProstoru.put("kraluvTrun",new Point2D(353,181));
        souradniceProstoru.put("sneznaOblast",new Point2D(161,143));
        souradniceProstoru.put("vodopad",new Point2D(26,230));
        souradniceProstoru.put("laborator",new Point2D(81,339));
        souradniceProstoru.put("trhlina",new Point2D(257,348));
        souradniceProstoru.put("dvere",new Point2D(196,243));
        return souradniceProstoru;
    }
    @FXML
    public void odeslaniVstupu(){
        String prikaz = vstup.getText();
        zpracujPrikaz(prikaz);
        vstup.clear();
    }

    private void zpracujPrikaz(String prikaz){
        vystup.appendText("Příkaz: "+prikaz+"\n");
        String vysledek= hra.zpracujPrikaz(prikaz);
        vystup.appendText(vysledek+"\n\n");

        if(hra.konecHry()){
            vystup.appendText(hra.vratEpilog());
            vstup.setDisable(true);
            odesli.setDisable(true);
            panelVychodu.setDisable(true);
        }


    }

    @Override
    public void update() {
        Prostor aktualniProstor = hra.getHerniPlan().getAktualniProstor();
        panelVychodu.getItems().clear();
        panelVychodu.getItems().addAll(hra.getHerniPlan().getAktualniProstor().getVychody());

        String nazevProstoru = aktualniProstor.getNazev();
        hrac.setLayoutX(souradniceProstoru.get(nazevProstoru).getX());
        hrac.setLayoutY(souradniceProstoru.get(nazevProstoru).getY());

        for (String vec : veci.keySet())
            if (hra.getBatoh().getVeci().containsKey(vec)){
                veci.get(vec).setVisible(true);
            }
            else{
                veci.get(vec).setVisible(false);
            }

    }

    public void vybranyVychod(MouseEvent mouseEvent) {
        Prostor vybranyProstor= (Prostor) panelVychodu.getSelectionModel().getSelectedItem();
        zpracujPrikaz("jdi"+" "+vybranyProstor.getNazev());
    }
}
