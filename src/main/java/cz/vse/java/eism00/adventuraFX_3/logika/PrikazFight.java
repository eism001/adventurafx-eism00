package cz.vse.java.eism00.adventuraFX_3.logika;

import java.util.Random;


public class PrikazFight implements IPrikaz {

    private static final String NAZEV = "fight";
    private Postava postava;
    private HerniPlan plan;

    public PrikazFight(Postava postava, HerniPlan plan) {
        this.postava = postava;
        this.plan = plan;
    }

    @Override
    public String provedPrikaz(String... parametry) {
        Random ra = new Random();
        int randomMAtk = ra.nextInt((plan.getAktualniProstor().getNpcVMístnosti().getAtk()+10)
                - plan.getAktualniProstor().getNpcVMístnosti().getAtk() + 1) + plan.getAktualniProstor().getNpcVMístnosti().getAtk();
        int randomAtk = ra.nextInt((postava.getAtk()+30)- postava.getAtk() + 1) + postava.getAtk();
        int novyAtk = postava.getAtk() + plan.getAktualniProstor().getNpcVMístnosti().getAtk();

        if (parametry.length != 0 ) {
            return "Stačí napsat příkaz fight, v místnosti není více postav na bojování";
        } else if (plan.getAktualniProstor().getNpcVMístnosti() == null){
            return "V lokaci se nenachází žádné NPC";
        } else if (plan.getAktualniProstor().getNpcVMístnosti().isProbehlFight()){
            return "S tímto monstrem už nemůžeš bojovat";
        } else if(!plan.getAktualniProstor().getNpcVMístnosti().isProbehlDialog()){
            return "Nejprve si promluv s NPC";
        }
        else {
            plan.getAktualniProstor().getNpcVMístnosti().setHealth(plan.getAktualniProstor().getNpcVMístnosti().getHealth()-randomAtk);
            postava.setHealth(postava.getHealth()-randomMAtk);
            if(plan.getAktualniProstor().getNpcVMístnosti().getHealth() > 0){
                if(randomAtk > (postava.getAtk()+20)){
                    return "WOW, cricital hit \n" +
                            "Udeřil si monstrum za " + randomAtk + " životů.\n"+
                            "Monstru zbývá "+ plan.getAktualniProstor().getNpcVMístnosti().getHealth() + " životů.\n" +
                            "Zbývá ti " + postava.getHealth() + " životů." ;
                }
                return "Udeřil si monstrum za "+ randomAtk + " životů.\n" +
                        "Monstru zbývá "+ plan.getAktualniProstor().getNpcVMístnosti().getHealth() + " životů.\n" +
                        "Zbývá ti "+ postava.getHealth() + " životů.";
            } else {
                postava.setAtk(novyAtk);
                plan.getAktualniProstor().getNpcVMístnosti().setProbehlFight(true);
            }
            return "Gratuluji k vyhrané bitvě, doufám že se teď cítíš lépe :) \n " +
                    "Tvůj nový útok je "+ postava.getAtk();
        }
    }


    @Override
    public String getNazev() {
        return NAZEV;
    }
}

