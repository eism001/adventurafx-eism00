package cz.vse.java.eism00.adventuraFX_3.logika;

public class PrikazHeal implements IPrikaz {

    private static final String NAZEV = "heal";
    private Postava postava;

    public PrikazHeal(Postava postava) {
        this.postava = postava;
    }

    @Override
    public String provedPrikaz(String... parametry) {
        int manaCost = 20;
        int pocetHeal = 50;
        int novaMana = postava.getMana()- manaCost;
        int novyHealth = postava.getHealth()+ pocetHeal;
        int zbylyHealth = 100 - postava.getHealth();
        if (parametry.length != 0) {
            return "Špatný příkaz, použij nápovědu.";
        }else if(postava.getMana() <20){
            return "Nemáš dostatek many na toto kouzlo";
        }else if(postava.getHealth() == 100) {
            return "Máš plný počet zdraví. Nemůžeš se vyhealovat.";
        }else if(novyHealth>100){
            postava.setHealth(100);
            return "Obnovil sis " + zbylyHealth + " životů.\n" +
                    "Nyní máš plný počet životů.";
        }else {
            postava.setMana(novaMana);
            postava.setHealth(novyHealth);
            return "Obnovil sis " + pocetHeal +" životů.\n" +
                    "Nyní máš " + novyHealth +" životů.";

        }
    }

    @Override
    public String getNazev() {
        return NAZEV;
    }
}