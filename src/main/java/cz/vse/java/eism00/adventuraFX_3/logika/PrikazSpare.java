package cz.vse.java.eism00.adventuraFX_3.logika;

class PrikazSpare implements IPrikaz {

    private static final String NAZEV = "spare";
    private Inventory inventory;
    private HerniPlan plan;

    public PrikazSpare(Inventory inventory, HerniPlan plan) {
        this.inventory = inventory;
        this.plan = plan;
    }

    @Override
    public String provedPrikaz(String... parametry) {
        int novyDuse = inventory.getPocetDusi() + plan.getAktualniProstor().getNpcVMístnosti().getDarDuse();
        if (parametry.length != 0 ) {
            return "Stačí napsat příkaz spare, v místnosti je pouze jedna osoba, kterou můžeš ušetřit.";
        }else if (plan.getAktualniProstor().getNpcVMístnosti() == null) {
            return "V lokaci se nenachází žádné NPC";
        } else if (plan.getAktualniProstor().getNpcVMístnosti().isProbehlFight() && getNazev().equals("spare") ){
            return "Toto monstrum jsi již ušetřil";
        } else if(!plan.getAktualniProstor().getNpcVMístnosti().isProbehlDialog()){
            return "Nejprve si promluv s NPC";
        } else {
            inventory.setPocetDusi(novyDuse);
            plan.getAktualniProstor().getNpcVMístnosti().setProbehlFight(true);
            return "Povedlo se ti zkrotit tvé vnitřní zvíře a nechal jsi tuto ubohou příšerku žít <3.\n" +
                    "Na oplátku jsi získal jeho duši.";
        }
    }


    @Override
    public String getNazev() {
        return NAZEV;
    }
}
