package cz.vse.java.eism00.adventuraFX_3.Main;

public interface Subject {

    void register(Observer observer);

    void unregister(Observer observer);

    void notifyObservers();

}
