package cz.vse.java.eism00.adventuraFX_3.logika;

public class PrikazSpeak implements IPrikaz {

    private static final String NAZEV = "speak";
    private  Postava postava;
    private HerniPlan plan;

    public PrikazSpeak(Postava postava, HerniPlan plan){
        this.postava = postava;
        this.plan = plan;
    }

    @Override
    public String provedPrikaz(String... parametry){
        if(parametry.length != 0 ){
            return "Špatně jsi zadal příkaz, použij nápovědu pokud si nevíš rady";
        } else if (plan.getAktualniProstor().getNpcVMístnosti() == null){
            return "V lokaci se nenachází žádné NPC";
        } else if (plan.getAktualniProstor().getNpcVMístnosti().isProbehlFight()){
            return "Dané NPC je dead, nelze s ním mluvit";
        } else if (plan.getAktualniProstor().getNpcVMístnosti().isProbehlDialog()){
            return "S touto postavou už jsi mluvil";
        } else {
            plan.getAktualniProstor().getNpcVMístnosti().setProbehlDialog(true);
            return plan.getAktualniProstor().getNpcVMístnosti().getDialog();
        }
    }

    @Override
    public String getNazev(){
        return NAZEV;
    }

}


