package cz.vse.java.eism00.adventuraFX_3.Main;

import cz.vse.java.eism00.adventuraFX_3.logika.Hra;
import cz.vse.java.eism00.adventuraFX_3.logika.IHra;
import cz.vse.java.eism00.adventuraFX_3.uiText.TextoveRozhrani;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Start extends Application {
    /***************************************************************************
     * Metoda, prostřednictvím níž se spouští celá aplikace.
     *
     * @param args Parametry příkazového řádku
     */
    public static void main(String[] args)
    {

        if(args.length==0){
            launch();
        }
        else if (args[0].equals("text") ) {
            IHra hra = new Hra();
            TextoveRozhrani ui = new TextoveRozhrani(hra);
            ui.hraj();
        }
    }

    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader loader= new FXMLLoader();
        loader.setLocation(getClass().getResource("/home.fxml"));
        GridPane root = loader.load();
        HomeController controller = loader.getController();
        stage.setScene(new Scene(root));
        stage.show();
    }
}