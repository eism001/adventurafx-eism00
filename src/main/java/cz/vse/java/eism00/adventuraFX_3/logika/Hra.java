package cz.vse.java.eism00.adventuraFX_3.logika;

public class Hra implements IHra {
    private SeznamPrikazu platnePrikazy;    // obsahuje seznam přípustných příkazů
    private HerniPlan herniPlan;
    private boolean konecHry = false;
    private Postava postava;
    private Inventory inventory;
    private Batoh batoh;


    /**
     *  Vytváří hru a inicializuje místnosti (prostřednictvím třídy HerniPlan) a seznam platných příkazů.
     */
    public Hra() {
        herniPlan = new HerniPlan();
        // Na zacatku hry má postava tyto staty
        postava = new Postava(20, 100,140);
        // Na zacatku hry je batoh prazdny
        inventory = new Inventory(0);
        platnePrikazy = new SeznamPrikazu();
        batoh = new Batoh();
        platnePrikazy.vlozPrikaz(new PrikazNapoveda(platnePrikazy));
        platnePrikazy.vlozPrikaz(new PrikazJdi(herniPlan));
        platnePrikazy.vlozPrikaz(new PrikazKonec(this));
        platnePrikazy.vlozPrikaz(new PrikazFight(postava, herniPlan));
        platnePrikazy.vlozPrikaz(new PrikazSpeak(postava, herniPlan));
        platnePrikazy.vlozPrikaz(new PrikazSpare(inventory, herniPlan));
        platnePrikazy.vlozPrikaz(new PrikazInventory(inventory));
        platnePrikazy.vlozPrikaz(new PrikazHeal(postava));
        platnePrikazy.vlozPrikaz(new PrikazStaty(postava));
        platnePrikazy.vlozPrikaz(new PrikazPoloz(herniPlan, batoh));
        platnePrikazy.vlozPrikaz(new PrikazSeber(herniPlan,batoh));
        platnePrikazy.vlozPrikaz(new PrikazObsahBatohu(batoh));
    }

    /**
     *  Vrátí úvodní zprávu pro hráče.
     */
    public String vratUvitani() {
        return "Vítej ve hře, která prověří tvé seběvědomí a morálku.\n" +
                "Probudil ses v místnosti v opuštěném hotelu, v zemi monster.\n" +
                "Tvým ukolem je se dostat zpátky domů.\n" +
                "Napište 'napoveda', pokud si nevíte rady, jak hrát dál. !!! Vřele doporučuji ;) .\n" +
                "\n" +
                herniPlan.getAktualniProstor().dlouhyPopis();
    }

    /**
     *  Vrátí závěrečnou zprávu pro hráče.
     */
    public String vratEpilog() {
        return "Děkuji, že jste si hru zahráli. Hra má více konců, tak zkuste dosáhnout všech.";
    }

    /**
     * Vrací true, pokud hra skončila.
     */
    public boolean konecHry() {
        return konecHry;
    }

    /**
     *  Metoda zpracuje řetězec uvedený jako parametr, rozdělí ho na slovo příkazu a další parametry.
     *  Pak otestuje zda příkaz je klíčovým slovem  např. jdi.
     *  Pokud ano spustí samotné provádění příkazu.
     *
     *@param  radek  text, který zadal uživatel jako příkaz do hry.
     *@return          vrací se řetězec, který se má vypsat na obrazovku
     */
    public String zpracujPrikaz(String radek) {
        String [] slova = radek.split("[ \t]+");
        String slovoPrikazu = slova[0];
        String []parametry = new String[slova.length-1];
        for(int i=0 ;i<parametry.length;i++){
            parametry[i]= slova[i+1];
        }
        String textKVypsani=" .... ";
        if (platnePrikazy.jePlatnyPrikaz(slovoPrikazu)) {
            IPrikaz prikaz = platnePrikazy.vratPrikaz(slovoPrikazu);
            textKVypsani = prikaz.provedPrikaz(parametry);
            if(postava.getHealth()<=0){
                textKVypsani = "Monstrum tě zabilo, tvoje cesta končí.";
                setKonecHry(true);
            }else if(herniPlan.getAktualniProstor().getNazev().equals("dvere") && (postava.getAtk() >= 170)){
                textKVypsani = "Stal jsi se jedním z monster, již není návratu zpět.\n" +
                        "Zůstaneš tu napořád.";
                setKonecHry(true);
            }
            else if (herniPlan.getAktualniProstor().getNazev().equals("dvere") && (inventory.getPocetDusi() == 6 )){
                textKVypsani = "Povedlo se ti nasbírat všechny duše a spasit zemi monster.\n" +
                        "Otevřel jsi tím bránu mezi Vašemi světy a nastolil jsi tak mír.";
                setKonecHry(true);
            }
            else if(herniPlan.getAktualniProstor().getNazev().equals("dvere")) {
                textKVypsani = "Úspěšně jsi ukončil hru, gratuluji.";
                setKonecHry(true);
            }
        }
        else {
            textKVypsani="Nevím co tím myslíš? Tento příkaz neznám. ";
        }
        return textKVypsani;
    }


    /**
     *  Nastaví, že je konec hry, metodu využívá třída PrikazKonec,
     *  mohou ji použít i další implementace rozhraní Prikaz.
     *
     *  @param  konecHry  hodnota false= konec hry, true = hra pokračuje
     */
    void setKonecHry(boolean konecHry) {
        this.konecHry = konecHry;
    }

    /**
     *  Metoda vrátí odkaz na herní plán, je využita hlavně v testech,
     *  kde se jejím prostřednictvím získává aktualní místnost hry.
     *
     *  @return     odkaz na herní plán
     */
    public HerniPlan getHerniPlan(){
        return herniPlan;
    }

}

