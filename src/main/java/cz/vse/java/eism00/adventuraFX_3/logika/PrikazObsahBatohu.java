package cz.vse.java.eism00.adventuraFX_3.logika;

public class PrikazObsahBatohu implements IPrikaz
{
    private static final String NAZEV = "obsahBatohu";
    private Batoh batoh;
    /**
     *  Konstruktor třídy
     *
     *  @param herniplan herní plán, ve kterém se bude hledat aktuální místnost
     */
    public PrikazObsahBatohu( Batoh batoh) {
        this.batoh = batoh;
    }
    /**
     *  Provádí příkaz "obsahBatohu". Vypíše názvy věcí v batohu
     *
     *@return zpráva, kterou vypíše hra hráči
     */
    public String provedPrikaz(String... parametry) {
        return batoh.nazvyVeci();
    }

    public String getNazev() {
        return NAZEV;
    }

}

