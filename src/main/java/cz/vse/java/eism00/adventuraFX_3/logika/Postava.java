package cz.vse.java.eism00.adventuraFX_3.logika;

public class Postava {
    private int atk;
    private int health;
    private int mana;

    public Postava(int atk, int health, int mana){
        this.atk = atk;
        this.health = health;
        this.mana = mana;
    }

    public void setAtk(int atk){
        this.atk = atk;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getMana() {
        return mana;
    }

    public void setMana(int mana) {
        this.mana = mana;
    }

    public int getAtk() {
        return atk;
    }


}